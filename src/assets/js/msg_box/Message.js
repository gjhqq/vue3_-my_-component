import { createApp } from "vue";

import message from '../../../components/msgbox/Message.vue';

// 创建div
let mountNode = document.createElement("div");
mountNode.setAttribute("class","message-list")

// 将div加入到body中
document.body.appendChild(mountNode);

// 成功提示
export function createMessage(data){
  let a = document.createElement("div");
  document.getElementsByClassName("message-list")[0].appendChild(a);


  let messageInstance = createApp(message,data)
  // 挂载messageInstanc到a
  messageInstance.mount(a);
  messageUnmount(messageInstance,a,data);
}

function messageUnmount(messageInstance,a,data){
  if(data.duration == undefined || data.duration == null){
    data.duration = 3000;
  }
  setTimeout(() => {
    messageInstance.unmount(a);
    a.remove();
  }, data.duration + 500);
}

export default createMessage;
