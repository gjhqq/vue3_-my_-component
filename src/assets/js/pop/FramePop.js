import { createApp } from "vue";
import framePop from '../../../components/modulmsg/FramePop.vue';

let div = null;
let popInstance = null;

function removePop(){
    popInstance.unmount(div)
    div.remove();
}
function createPop(pop_data){
    div = document.createElement("div");
    div.setAttribute("class","frame-pop");
    document.body.appendChild(div);


    popInstance = createApp(framePop,pop_data);
    popInstance.mount(div)
}

export default {removePop,createPop};