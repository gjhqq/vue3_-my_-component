import { createApp } from "vue";
import frameEmptyPop from '../../../components/modulmsg/FrameEmptyPop.vue';

let div = null;
let popInstance = null;

function removeEmptyPop(){
    popInstance.unmount(div)
    div.remove();
}
function createEmptyPop(empty_pop_data){
    div = document.createElement("div");
    div.setAttribute("class","frame-pop");
    document.body.appendChild(div);


    popInstance = createApp(frameEmptyPop,empty_pop_data);
    popInstance.mount(div)
}

export default {removeEmptyPop,createEmptyPop};