/**
 * @param {Object} type 返回类型 1:2022-2-6；2:2022-02-06
 */
export function getNowTime(type){
    let date = new Date();
  			
	let year = date.getFullYear();        //年 ,从 Date 对象以四位数字返回年份
	let month = date.getMonth() + 1;      //月 ,从 Date 对象返回月份 (0 ~ 11) ,date.getMonth()比实际月份少 1 个月
	let day = date.getDate();             //日 ,从 Date 对象返回一个月中的某一天 (1 ~ 31)
	
	let hours = date.getHours();          //小时 ,返回 Date 对象的小时 (0 ~ 23)
	let minutes = date.getMinutes();      //分钟 ,返回 Date 对象的分钟 (0 ~ 59)
	let seconds = date.getSeconds();      //秒 ,返回 Date 对象的秒数 (0 ~ 59)   
	
	//获取当前系统时间  
	if(type === 1){
		let currentDate = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
		return currentDate;
	}else if(type === 2){
		//修改月份格式
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		//修改日期格式
		if (day >= 0 && day <= 9) {
			day = "0" + day;
		}
		//修改小时格式
		if (hours >= 0 && hours <= 9) {
			hours = "0" + hours;
		}
		//修改分钟格式
		if (minutes >= 0 && minutes <= 9) {
			minutes = "0" + minutes;
		}
		//修改秒格式
		if (seconds >= 0 && seconds <= 9) {
			seconds = "0" + seconds;
		}
		
		//获取当前系统时间  格式(yyyy-mm-dd hh:mm:ss)
		let currentFormatDate = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
		return currentFormatDate;
	}else{
		//修改小时格式
		if (hours >= 0 && hours <= 9) {
			hours = "0" + hours;
		}
		//修改分钟格式
		if (minutes >= 0 && minutes <= 9) {
			minutes = "0" + minutes;
		}
		//修改秒格式
		if (seconds >= 0 && seconds <= 9) {
			seconds = "0" + seconds;
		}
		let currentFormatDate = hours + ":" + minutes + ":" + seconds;
		return currentFormatDate;
	}
}

/**
 * @param {Object} array 数组数据
 * @return {Object} array为null则返回空[]。否则返回原array
 */
export function isArrayEmpty(array){
	if(array == null){
		let newArray = [];
		return newArray;
	}
	return array;
}

/**
 * @param {Object} data 发送axiso请求的数据
 * @return {Object} data为null则返回 {}，否则返回原有data
 */
export function isAxisoDataEmpay(data){
	if(data == null){
		return {};
	}
	return data;
}


// 设置消息显示时间
export function setTime(time){
    if (time == 'undefined' || time == null || time == 'null') {
        return 3000;
    }else if(time == 'normal'){
        return 3000;
    }else if(time == 'short'){
        return 2000;
    }else if(time == 'long'){
        return 5000;
    }
}


export function isArrayEmptyBool(data){
	if(data != null && data != undefined && data.length > 0){
		return true;
	}
	return false;
}

export function isObjectEmptyBool(data){
	if(data != null && data != undefined && data != {}){
		return true;
	}
	return false;
}