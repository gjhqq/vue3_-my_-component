// 引入push.js浏览器桌面消息推送
import push from 'push.js';


export function ok(){
    customNotice("操作完成",{body:"请求完成，操作成功"});
}
export function faild(){
    customNotice("操作失败",{body:"请求未完成，操作失败，请重试"});
}
/**
 * 发送系统消息
 * @param {消息内容} body String
 * @param {是否系统提示音} silent boolean
 */
export function systemNotice(body,silent){
    push.create("系统通知", {
        body: body,
        silent:silent
    });
}
/**
 * 
 * @param {提示标题} title String
 * @param {自定义option} param Object
 */
export function customNotice(title,param){
    push.create(title, param);
}

 export function clear(){
    push.clear();
}
