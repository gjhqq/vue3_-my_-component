import { createRouter, createWebHistory } from 'vue-router';

// const testView00 = ()=>import('@/view/TestView00.vue')
const testView01 = ()=>import('@/view/TestView01.vue')
const testView02 = ()=>import('@/view/TestView02.vue')
const testView03 = ()=>import('@/view/TestView03.vue')
const testView04 = ()=>import('@/view/TestView04.vue')
const routes = [
  {
    path:'/',
    direct:'test04',
    component:testView04
  },
  // {
  //   path:'/test00',
  //   component:testView00
  // },
  {
    path:'/test01',
    component:testView01
  },
  {
    path:'/test02',
    component:testView02
  },
  {
    path:'/test03',
    component:testView03
  },
  {
    path:'/test04',
    component:testView04
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
