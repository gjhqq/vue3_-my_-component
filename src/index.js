import framePop from './components/modulmsg/FramePop.vue';
import frameFormPop from './components/modulmsg/FrameFormPop.vue';
import drawer from './components/modulmsg/Drawer.vue';
import message from './components/msgbox/Message.vue';
import simpleAlert from './components/alert/SimpleAlert.vue';
import crumb from './components/crumb/BreadCrumb.vue';
import progressBar from './components/progress/ProgressBar.vue';
import selectDown from './components/select/SelectDown.vue';
import blockNext from './components/select/BlockNext.vue';
import mTree from './components/tree/MenuTree.vue';
import fTable from './components/table/FrameTable.vue';
import quote from './components/common/Quote.vue';

import form from './components/form/Form.vue';
import inputItem from './components/form/form_item/InputItem.vue';
import labelItem from './components/form/form_item/LabelItem.vue';
import selectItem from './components/form/form_item/SelectItem.vue';


import emptyPopCom from './components/modulmsg/FrameEmptyPop.vue';


import createMessage from './assets/js/msg_box/Message.js';
import pop from './assets/js/pop/FramePop.js';
import emptyPop from './assets/js/pop/FrameEmptyPop.js';

import oneToTwo from './components/grid/OneToTwo.vue';
import simplepage from './components/page/SimplePage.vue';
import onoff from './components/onoff/Onoff.vue';
import skeleton from './components/skeleton/Skeleton.vue';
import imgSkeleton from './components/skeleton/ImgSkeleton.vue';
import tag from './components/tag/Tag.vue';
import badge from './components/tag/Badge.vue';
import toolTips from './components/tips/ToolTips.vue';
import carousel from './components/carousel/Carousel.vue';
import timeNode from './components/time_node/TimeNode.vue';


let componentList = [
    simpleAlert,
    crumb,
    progressBar,
    selectDown,
    mTree,
    fTable,
    framePop,
    frameFormPop,
    form,
    inputItem,
    labelItem,
    selectItem,
    quote,
    drawer,
    message,
    oneToTwo,
    simplepage,
    onoff,
    skeleton,
    imgSkeleton,
    tag,
    blockNext,
    badge,
    toolTips,
    carousel,
    emptyPopCom,
    timeNode
]

// 注册全局组件
let com = {
    install(app){
        componentList.forEach(n=>{
            app.component(n.name,n);
        })
        app.config.globalProperties.$createMessage = createMessage;
        app.config.globalProperties.$createPop = pop.createPop;
        app.config.globalProperties.$removePop = pop.removePop;
        app.config.globalProperties.$createEmptyPop = emptyPop.createEmptyPop;
        app.config.globalProperties.$removeEmptyPop = emptyPop.removeEmptyPop;

        

    }
    

}


export {com};
  
  