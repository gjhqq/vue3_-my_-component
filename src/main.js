import { createApp } from 'vue'

import App from './App.vue';
import router from './router';

import {com} from '@/index.js';


let app = createApp(App);
app.use(com)
app.use(router)
    .mount('#app');
