## G-Frame组件库
### 说明文档：
http://www.petadoption.work:10010/
### gitee地址访问地址：
https://gitee.com/gjhqq/vue3_-my_-component
### git地址：
https://gitee.com/gjhqq/vue3_-my_-component.git
### 描述
1. 此组件库是个人为了学习而开发的，借鉴了element ui和bootstrap两个前端框架。
2. 底层代码基于vue3.x。
3. 并且收录了ElementUI，BootStrap，iView三个前端组件库的主题颜色，方便直接参考和使用
### 通过npm进行安装
1.组件已上传至npm，可以通过npm进行下载
``` shell
npm install cli13-vue3
```
2.使用的话只需要通过在main.js中引入
```javascript
import {com} from 'cli13-vue3/src';
app.use(store).use(com)
```

### 包含组件

**组件文档**
- 消息弹窗
- 警告框alert
- 模态框
- select下拉框
- 表单弹窗
- 进度条
- 表格
- 开关组件
- 按钮
- 面包屑
- tag标签
- 块级选择
- 加载框架

**系统内置**
- 内置样式
- 内置动画

**手册**
- easyTool工具类操作手册

**主题**
- ElementUI
- BootStrap
- iView